    // swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "dotveep dynamic xcfamework",
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "dotveep",
            targets: ["dotveep"]),
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
//        .binaryTarget(name:"dotveep", path:"../dotveep2/xcframeworks/dotveep-static/dotveep.xcframework" ),
        .binaryTarget(
            name: "dotveep",
            url: "https://gitlab.com/foundry/dotveep/-/raw/2.0.1/xcframeworks/dotveep-dynamic/dotveep.xcframework.zip",
            checksum: "865b74e0f931e9027203e4a29bdbbb9d486efb0ab3425b9fd63fc39d32fee637")
        
    ]
)
